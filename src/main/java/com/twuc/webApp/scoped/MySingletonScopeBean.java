package com.twuc.webApp.scoped;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class MySingletonScopeBean {
    private static int count = 0;

    public static int getCount() {
        return count;
    }

    public MySingletonScopeBean() {
        count++;
    }
}
