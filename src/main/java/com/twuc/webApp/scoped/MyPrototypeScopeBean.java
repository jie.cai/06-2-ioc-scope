package com.twuc.webApp.scoped;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MyPrototypeScopeBean {
    private static int count = 0;

    public static int getCount() {
        return count;
    }

    public MyPrototypeScopeBean() {
        count++;
    }
}
