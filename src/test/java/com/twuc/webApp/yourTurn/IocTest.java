package com.twuc.webApp.yourTurn;

import com.twuc.webApp.scoped.MyPrototypeScopeBean;
import com.twuc.webApp.scoped.MySingletonScopeBean;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IocTest {

    private AnnotationConfigApplicationContext normalContext() {
        return new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    @Test
    void should_get_same_instance_bean() {
        AnnotationConfigApplicationContext context = normalContext();
        InterfaceOne bean1 = context.getBean(InterfaceOne.class);
        InterfaceOne bean2 = context.getBean(InterfaceOneImpl.class);

        assertSame(bean1, bean2);
    }

    @Test
    void should_throw_exception_when_get_same_instance_bean_by_extent() {
        assertThrows(Exception.class, () ->  normalContext().getBean(Parent.class));
    }

    @Test
    void should_get_same_instance_bean_by_extent_abstract() {
        AnnotationConfigApplicationContext context = normalContext();
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        AbstractBaseClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstractBaseClass, derivedClass);
    }

    @Test
    void should_always_get_new_bean_by_scope_annotation() {
        AnnotationConfigApplicationContext context = normalContext();

        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean2 = context.getBean(SimplePrototypeScopeClass.class);

        assertNotSame(bean1, bean2);
    }

    @Test
    void should_create_bean_when_get_bean_by_scope() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.scoped");

        assertEquals(1, MySingletonScopeBean.getCount());
        context.getBean(MySingletonScopeBean.class);
        context.getBean(MySingletonScopeBean.class);
        context.getBean(MySingletonScopeBean.class);
        assertEquals(1, MySingletonScopeBean.getCount());

        assertEquals(0, MyPrototypeScopeBean.getCount());
        context.getBean(MyPrototypeScopeBean.class);
        context.getBean(MyPrototypeScopeBean.class);
        context.getBean(MyPrototypeScopeBean.class);
        assertEquals(3, MyPrototypeScopeBean.getCount());
    }

//    @Test
//    void should_get_bean_of_singleton() {
//
//    }
}
